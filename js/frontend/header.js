$(document).ready(function() {
  let header = document.querySelector('.b-header');

  if ($(window).innerWidth() < 1025) {
    return false
  }
  let fixedHeader = $('.js-fixed-header');
  if (header) {
    $(window).on('scroll', function() {
      let rect = header.getBoundingClientRect().top;

      if (rect > -124) {
        fixedHeader.removeClass('active');
        return
      }
      fixedHeader.addClass('active');
    });
  }
});

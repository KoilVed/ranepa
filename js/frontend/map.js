$(window).on('load', function() {
  if ($('#map-contact').length) {
    ymaps.ready(function() {
      var myMapD = new ymaps.Map('map-contact', {
        center  : [53.331550, 83.765806],
        zoom    : 16,
        controls: []
      });
      let content = ymaps.templateLayoutFactory.createClass(
        '<div class="map-icon"></div>'
      );
      let placemark = new ymaps.Placemark([53.331550, 83.765806], {}, {
        iconLayout       : 'default#imageWithContent',
        iconImageHref    : '',
        iconImageSize    : [50, 60],
        iconImageOffset  : [-30, -70],
        iconContentOffset: [15, 15],
        iconContentLayout: content
      });
      myMapD.geoObjects.add(placemark);
      myMapD.behaviors.disable('scrollZoom');
    });
  }
});

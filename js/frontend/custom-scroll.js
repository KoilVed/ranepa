require('jquery-mousewheel');
require('malihu-custom-scrollbar-plugin');

$(document).ready(function() {
  $('.js-scrollbar-table').mCustomScrollbar({
    axis             : 'x',
    scrollbarPosition: 'inside',
    theme            : 'table-theme'
  });
});

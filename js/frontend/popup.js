let magnificPopup = require('magnific-popup');
import slick from 'slick-carousel';

$(document).ready(function() {
  $('.js-popup').magnificPopup({
    fixedContentPos: true,
    preloader      : false,
    mainClass      : 'audience-popup',
    callbacks      : {
      open: function() {
        $('.js-audience-slider').slick('reinit')

        $.each($('.b-audience-slider__img'), function(index, item) {
          let $item = $(item)
          let image = $item.find('img')
          let svg = $item.find('svg')
          let width = image.width()
          let height = image.height()

          svg.css({'width': width, 'height': height});
        })
      },
      beforeClose: function() {
      }
    }
  });
  $('.js-popup-question').magnificPopup({
    fixedContentPos: true,
    preloader      : false,
    mainClass      : 'question-popup'
  });
});

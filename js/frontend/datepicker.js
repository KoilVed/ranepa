import 'air-datepicker';

$(document).ready(function() {
  let eventDates = [1, 9, 12, 16, 22];
  let eventMonth = [3];
  let datepicker =  $('.js-calendar').datepicker({
    position    : 'bottom right',
    onRenderCell: function(date, cellType) {
      let currentDate = date.getDate();
      let currentMonth = date.getMonth();

      if (cellType == 'day' && eventDates.indexOf(currentDate) != -1 && eventMonth.indexOf(currentMonth) != -1) {
        return {
          html   : currentDate + '<span class="dp-note"></span>',
          classes: 'only-active'
        }
      }
    }
  }).data('datepicker');
});

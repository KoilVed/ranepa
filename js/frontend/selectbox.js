$(document).ready(function () {
  let selectbox = $('.js-selectbox')

  selectbox.on('click', function () {
    let $this = $(this);
    let selected = $this.find('.selectbox__selected')
    let dropdown = $this.find('.selectbox__dropdown')
    let options = $this.find('.selectbox__option')
    $('.selectbox__dropdown').removeClass('active')
    dropdown.toggleClass('active')

    options.on('click', function (e) {
      let $this = $(this);
      options.removeClass('active')
      $this.addClass('active')
      let textOption = $this.text()
      selected.text(textOption)
      dropdown.removeClass('active')
      e.stopPropagation()
    });
  });

  $(document).click(function (event) {
    if ($(event.target).closest('.js-selectbox').length) return;

    $('.selectbox__dropdown').removeClass('active')
  });
});

/*! jquery.cookie v1.4.1 | MIT */
!function (a) {
  typeof define == 'function' && define.amd ? define(['jquery'], a) : typeof exports == 'object' ? a(require('jquery')) : a(jQuery)
}(function (a) {
  function b (a) {
    return h.raw ? a : encodeURIComponent(a)
  }

  function c (a) {
    return h.raw ? a : decodeURIComponent(a)
  }

  function d (a) {
    return b(h.json ? JSON.stringify(a) : String(a))
  }

  function e (a) {
    a.indexOf('"') === 0 && (a = a.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\'));
    try {
      return a = decodeURIComponent(a.replace(g, ' ')), h.json ? JSON.parse(a) : a
    } catch (b) {
    }
  }

  function f (b, c) {
    var d = h.raw ? b : e(b);
    return a.isFunction(c) ? c(d) : d
  }

  var g = /\+/g,
    h = a.cookie = function (e, g, i) {
      if (void 0 !== g && !a.isFunction(g)) {
        if (i = a.extend({}, h.defaults, i), typeof i.expires == 'number') {
          var j = i.expires,
            k = i.expires = new Date;
          k.setTime(+k + 864e5 * j)
        }
        return document.cookie = [b(e), '=', d(g), i.expires ? '; expires=' + i.expires.toUTCString() : '', i.path ? '; path=' + i.path : '', i.domain ? '; domain=' + i.domain : '', i.secure ? '; secure' : ''].join('')
      }
      for (var l = e ? void 0 : {}, m = document.cookie ? document.cookie.split('; ') : [], n = 0, o = m.length; o > n; n++) {
        var p = m[n].split('='),
          q = c(p.shift()),
          r = p.join('=');
        if (e && e === q) {
          l = f(r, g);
          break
        }
        e || void 0 === (r = f(r)) || (l[q] = r)
      }
      return l
    };
  h.defaults = {}, a.removeCookie = function (b, c) {
    return void 0 === a.cookie(b) ? !1 : (a.cookie(b, '', a.extend({}, c, {
      expires: -1
    })), !a.cookie(b))
  }
});

$(document).ready(function () {
  let bodyTag = $('body');
  let zoomEl = $('.js-zoom-visually');
  zoomEl.click(function () {
    let $this = $(this);
    let data = $this.attr('data-zoom')
    zoomEl.removeClass('active')
    bodyTag.removeClass('size-1 size-2 size-3')
    $this.addClass('active')

    if (Number(data) === 1) {
      bodyTag.addClass('size-1')
      $.cookie('size', 'size-1');
    }
    if (Number(data) === 2) {
      bodyTag.addClass('size-2')
      $.cookie('size', 'size-2');
    }
    if (Number(data) === 3) {
      bodyTag.addClass('size-3')
      $.cookie('size', 'size-3');
    }
    $('.slick-slider').slick('setPosition');
  });


  let colorEl = $('.js-color-visually');
  colorEl.click(function () {
    let $this = $(this);
    let data = $this.attr('data-color')
    colorEl.removeClass('active')
    bodyTag.removeClass('grayscale white_black blue_black')
    $this.addClass('active')

    if (Number(data) === 1) {
      bodyTag.removeClass('grayscale white_black blue_black')
      $.cookie('color', 'default');
    }
    if (Number(data) === 1) {
      bodyTag.addClass('grayscale')
      $.cookie('color', 'grayscale');
    }
    if (Number(data) === 2) {
      bodyTag.addClass('white_black')
      $.cookie('color', 'white_black');
    }
    if (Number(data) === 3) {
      bodyTag.addClass('blue_black')
      $.cookie('color', 'blue_black');
    }
  });

  $('.js-img-outer').click(function () {
    if ($.cookie('img') != 'on') {
      $('img').css('display', 'none');
      $.cookie('img', 'on');
      $(this).addClass('active');
    } else {
      $('img').css('display', 'block');
      $.cookie('img', 'off');
      $(this).removeClass('active');
    }
  });

  function blackState () {
    colorEl[1].click()
    zoomEl[1].click()
  }

  function defaultState () {
    $('.js-img-outer').removeClass('active');
    $.cookie('img', 'off');
    $('img').css('display', 'block');

    colorEl.removeClass('active')
    bodyTag.removeClass('grayscale white_black blue_black')

    zoomEl.removeClass('active')
    bodyTag.removeClass('size-1 size-2 size-3')
    $('.slick-slider').slick('setPosition');
  }

  $('.b-header__visually').on('click', function () {
    if ($(this).hasClass('active')) {
      defaultState()
    } else {
      blackState()
    }
    $(this).toggleClass('active')
    $('.b-header__eye').toggleClass('active')
    $('.b-header').toggleClass('is-visually')
  });

  $('.b-header__eye-close').on('click', function () {
    $('.b-header__visually').removeClass('active')
    $('.b-header__eye').removeClass('active')
    $('.b-header').removeClass('is-visually')
    defaultState()
  });
});

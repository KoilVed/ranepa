$(document).ready(function() {

  $('.js-anchor-doc > li').on('click touchstart', function(event) {
    event.preventDefault();
    let link = $(this).find('a').attr('href')
    var dest = $(link).offset().top;

    //go to destination
    $('html,body').animate({
      scrollTop: dest - 116
    }, 600, 'linear');
  });
});

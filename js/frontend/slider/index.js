import slick from 'slick-carousel';

let fullPageSlider = $('.js-fullpage-slider');
let homeSlider = $('.js-home-slider');
let homeProjectSlider = $('.js-home-slider--project');
let typographySlider = $('.js-slider-typography');
let audienceSlider = $('.js-audience-slider');
let tabletGallerySlider = $('.js-home-tablet-gallery');
let mobileGallerySlider = $('.js-home-mobile-gallery');
let aboutSlider = $('.js-about-slider');
let bannerSlider = $('.js-banner-slider');

let paramBannerSlider = {
  slidesToShow  : 1,
  slidesToScroll: 1,
  arrows        : true,
  infinite      : true,
  dots          : false,
  responsive    : [
    {
      breakpoint: 1367,
      settings  : {
        arrows: false,
        dots  : true
      }
    }
  ]
};
let paramAboutSlider = {
  slidesToShow  : 1,
  slidesToScroll: 1,
  arrows        : false,
  infinite      : false,
  dots          : true,
  mobileFirst   : true,
  responsive    : [
    {
      breakpoint: 840,
      settings  : 'unslick'
    },
    {
      breakpoint: 541,
      settings  : {
        slidesToShow: 2,
        arrows      : false,
        dots        : true
      }
    }
  ]
};

let paramAudienceSlider = {
  slidesToShow  : 1,
  slidesToScroll: 1,
  arrows        : true,
  infinite      : false,
  dots          : false,
  responsive    : [
    {
      breakpoint: 900,
      settings  : {
        arrows: false,
        dots  : true
      }
    }
  ]
};
let paramTypographySlider = {
  slidesToShow  : 1,
  slidesToScroll: 1,
  arrows        : true,
  infinite      : true,
  dots          : true,
  responsive    : [
    {
      breakpoint: 640,
      settings  : {
        arrows: false,
        dots  : true
      }
    }
  ]
};
let paramHomeSlider = {
  slidesToShow  : 4,
  slidesToScroll: 1,
  arrows        : true,
  infinite      : false,
  swipeToSlide  : true,
  responsive    : [
    {
      breakpoint: 1367,
      settings  : {
        arrows: false,
        dots  : true
      }
    },
    {
      breakpoint: 1025,
      settings  : {
        slidesToShow: 3,
        arrows      : false,
        dots        : true
      }
    },
    {
      breakpoint: 981,
      settings  : {
        slidesToShow: 2,
        arrows      : false,
        dots        : true
      }
    },
    {
      breakpoint: 541,
      settings  : {
        slidesToShow: 1,
        arrows      : false,
        dots        : true
      }
    }
  ]
};
let paramHomeProjectSlider = {
  slidesToShow  : 3,
  slidesToScroll: 1,
  arrows        : true,
  infinite      : false,
  swipeToSlide  : true,
  responsive    : [
    {
      breakpoint: 1366,
      settings  : {
        arrows: false,
        dots  : true
      }
    },
    {
      breakpoint: 980,
      settings  : {
        slidesToShow: 2,
        arrows      : false,
        dots        : true
      }
    },
    {
      breakpoint: 641,
      settings  : {
        slidesToShow: 1,
        arrows      : false,
        dots        : true
      }
    }

  ]
};

let paramFullPageSlider = {
  dots        : true,
  infinite    : false,
  speed       : 450,
  slidesToShow: 1,
  fade        : true,
  cssEase     : 'ease',
  arrows      : false
};

let paramTabletGallerySlider = {
  slidesToShow  : 1,
  slidesToScroll: 1,
  mobileFirst   : true,
  dots          : true,
  swipeToSlide  : true,
  responsive    : [
    {
      breakpoint: 1024,
      settings  : 'unslick'
    },
    {
      breakpoint: 768,
      settings  : {
        slidesToShow: 3,
        arrows      : false,
        dots        : true
      }
    },
    {
      breakpoint: 541,
      settings  : {
        slidesToShow: 2,
        arrows      : false,
        dots        : true
      }
    }
  ]
};

let paramMobileGallerySlider = {
  slidesToShow  : 1,
  slidesToScroll: 1,
  mobileFirst   : true,
  dots          : true,
  swipeToSlide  : true,
  responsive    : [
    {
      breakpoint: 641,
      settings  : 'unslick'
    },
    {
      breakpoint: 541,
      settings  : {
        slidesToShow: 2,
        arrows      : false,
        dots        : true
      }
    }
  ]
};


$(document).ready(function() {
  $(fullPageSlider).slick(paramFullPageSlider);
  $(homeSlider).slick(paramHomeSlider);
  $(homeProjectSlider).slick(paramHomeProjectSlider);
  $(typographySlider).slick(paramTypographySlider);
  $(audienceSlider).slick(paramAudienceSlider);
  $(tabletGallerySlider).slick(paramTabletGallerySlider);
  $(mobileGallerySlider).slick(paramMobileGallerySlider);
  $(aboutSlider).slick(paramAboutSlider);
  $(bannerSlider).slick(paramBannerSlider);
});

$(document).ready(function() {
  /**
   * кнопка скролла до 2 блока
   */
  let numberScrollHome = 80
  if (window.matchMedia('(max-width: 1025px)').matches) {
    numberScrollHome = 0
  }

  $('.js-to-anchor').on('click', function() {
    $('html, body').animate({scrollTop: $('#anchor-first').offset().top - numberScrollHome}, 400);
    return false;
  })

  $('.js-aside-filter').on('click', function() {
    let $this = $(this);
    $this.toggleClass('active')
    $this.closest('.page').find('aside').slideToggle()
  });
  /**
   * кнопка скролла на вверх
   */
  let btnScroll = $('.js-scrolltop')
  btnScroll.on('click', function() {
    $('html, body').animate({scrollTop: 0}, 600);
    return false;
  });
  /**
   * раскрытия дропдауна в расписание(таблица)
   */
  $('.js-toggle-desc').on('click', function() {
    let $this = $(this);
    let rowTable = $this.closest('.b-schedule__table-row')
    $this.toggleClass('active')
    rowTable.find('.b-schedule__table-wrap').toggleClass('active')
    rowTable.find('.b-schedule__table-dropdown').slideToggle('fast')
  });

  /**
   * тултип для слайдера в аудиториях
   */
  let tooltip = $('.tooltip-audience')
  tooltip.mouseleave(function() {
    $('.js-hover-polygon > g').removeClass('active')
    $(this).hide()
    $(this).text('')
  });

  $('.js-hover-polygon > g').hover(function() {
    let $this = $(this);
    let text = $this.find('polygon').attr('data-title');
    let top = $this.offset().top - 35;
    let left = $this.offset().left;
    $this.addClass('active')
    tooltip.text(text)
    let wTooltip = tooltip.innerWidth()/2
    let wPolygon = $this.find('polygon')[0].getBoundingClientRect().width/2
    tooltip.css({'top': `${top}px`, 'left': `${left - wTooltip + wPolygon}px`});
    tooltip.show()
  }, function(event) {
    if (!($(event.relatedTarget).attr('class') == 'tooltip-audience')) {
      let $this = $(this);
      $this.removeClass('active')
      tooltip.hide()
      tooltip.text('')
    }
  });
  /**
   * тултип для расписания
   */
  let tooltipTable = $('.tooltip-table')
  tooltipTable.mouseleave(function() {
    $(this).hide()
  });
  $('.js-hover-tooltip').hover(function() {
    let $this = $(this);
    let top = $this.offset().top - 43;
    let left = $this.offset().left;
    let wTooltip = tooltipTable.innerWidth()/2;
    let wIcon = $this.width()/2;
    let mobileLeft = 0;
    if (window.matchMedia('(max-width: 641px)').matches) {
      mobileLeft = 97
    }
    tooltipTable.css({'top': `${top}px`, 'left': `${left - wTooltip + wIcon + mobileLeft}px`});
    tooltipTable.show()
  }, function() {
    if (!($(event.relatedTarget).attr('class') == 'tooltip-table')) {
      tooltipTable.hide()
    }
  });
  /**
   * поиск в хедере
   */
  $('.js-header-search').on('click', function() {
    let $this = $(this);
    $this.closest('.b-header__search').find('.b-header__search-box').addClass('active')
    setTimeout(function() {
      $this.closest('.b-header__search').find('input').focus()
    }, 200)
  });

  $(document).click(function(event) {
    if ($(event.target).closest('.js-header-search').length) {
      return;
    }
    if ($(event.target).closest('.b-header__search-box').length) {
      return;
    }
    if ($(event.target).closest('.js-random-click').length) {
      return;
    }

    $('.b-header__search-box').removeClass('active')
  });

  $('.js-close-header-search').on('click', function() {
    $(this).closest('.b-header__search').find('.b-header__search-box').removeClass('active')
    $(this).closest('.b-header__search').find('input').val('')
  });
  /**
   * бургер меню
   */
  let clickDelay = 500;
  let clickDelayTimer = null;

  $('.burger-menu').on('click', function() {
    let body = $('html, body');
    let animationNumber = 300;
    let top = body.scrollTop()

    if (top < 3) {
      animationNumber = 0
    }

    body.stop().animate({scrollTop: 0}, animationNumber, 'swing', function() {
      if (clickDelayTimer === null) {
        let $burger = $(this);
        $burger.toggleClass('active');
        $('.mobile-menu').toggleClass('active');
        $('body').toggleClass('lock-position');

        if (!$burger.hasClass('active')) {
          $burger.addClass('closing');
        }

        clickDelayTimer = setTimeout(function() {
          $burger.removeClass('closing');
          clearTimeout(clickDelayTimer);
          clickDelayTimer = null;
        }, clickDelay);
      }
    });
  });
  /**
   * печать расписания
   */
  $('.js-print').on('click', function() {
    window.print();
  });

  if (window.matchMedia('(min-width: 1025px)').matches) {
    if ($('.b-main-tab').length) {
      let count = 0
      let flagStop = false
      setInterval(function() {
        if (flagStop) {
          return
        }
        $('.js-random-click > div')[count].click()
        if (count === 2) {
          count = 0
          return
        }
        count += 1
      }, 3000);

      $('.b-main-tab').on('mouseenter', function() {
        flagStop = true
      });
      $('.b-main-tab').on('mouseleave', function() {
        flagStop = false
      });
    }
  }
  /**
   * октрытия меню в aside
   */
  $('.js-aside-menu > a').on('click', function(e) {
    e.preventDefault()
    $(this).parent().toggleClass('change-arrow')
    $(this).next().slideToggle()
  });


  $(window).scroll(function() {
    if ($(this).scrollTop() > 500) {
      btnScroll.addClass('scroll-top--active');
    } else {
      btnScroll.removeClass('scroll-top--active');
    }
  });
  /**
   * Логика фильтра
   */
  const valueAll = '1' // значения для Все(Всего)
  $('.column-radio__item > input').change(function() {
    let $this = $(this);
    let value = $this.val();
    let btn = $('.js-aside-filter');
    if (value !== valueAll) {
      btn.addClass('select')
    } else {
      btn.removeClass('select')
    }
  });

  $('.column-checkbox__item > input').change(function() {
    let $this = $(this);
    let btn = $('.js-aside-filter');

    if ($this.prop('checked')) {
      btn.addClass('select')
    } else {
      btn.removeClass('select')
    }

    let checkedItem = $('.column-checkbox__item > input').filter((i, item) => {
      return $(item).prop('checked')
    })

    if (checkedItem.length !== 0) {
      btn.addClass('select')
    } else {
      btn.removeClass('select')
    }
  });
});

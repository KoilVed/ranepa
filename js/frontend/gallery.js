let magnificPopup = require('magnific-popup');
import PhotoSwipe from 'photoswipe'
import PhotoSwipeUI_Default from 'photoswipe/dist/photoswipe-ui-default'

$(document).ready(function () {
  var pswpElement = document.querySelectorAll('.pswp')[0];
  var items = [];

  $('.js-gallery > a').each(function(){
    var size = $(this).attr('data-size').split('x');
    items.push({
      src: $(this).attr('href'),
      w: size[0],
      h: size[1]
    });
  });


  $('.js-gallery > a').click(function(e){
    e.preventDefault()
    var options
    if (window.matchMedia('(max-width: 1025px)').matches) {
      options = {
        index: $(this).index(),
      };
    } else {
      options = {
        index: $(this).index(),
        closeOnVerticalDrag: true,
        clickToCloseNonZoomable: false,
        tapToClose: false,
      };
    }

    var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();
    return false;
  });
});

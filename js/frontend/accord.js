var Accordion = function (el,  className, multiple, itemClass,) {
  this.el = el || {};
  this.itemClass = itemClass || '.js-accordion';
  this.className = className || '';
  this.multiple = multiple || false;

  // Variables privadas
  var links = this.el.find(this.itemClass);
  // Evento
  links.on('click', {el: this.el, className: this.className, multiple: this.multiple}, this.dropdown)
}

Accordion.prototype.dropdown = function (e) {
  var $el = e.data.el;
  var $this = $(this),
    $next = $this.next();

  $next.slideToggle();
  $this.parent().toggleClass('open');

  if (!e.data.multiple) {
    $el.find(e.data.className).not($next).slideUp().parent().removeClass('open');
  }
  ;
}

$(document).ready(function () {
  var accordion = new Accordion($('#accordion'), '.b-accordion__content', false);
  var accordionFooter = new Accordion($('#accordion-footer'), 'ul', false);
  var accordionMenu = new Accordion($('#accordion-menu'), '.b-header__dropdown', false);
  var accordionEducation = new Accordion($('#accordion-education'), '.b-main-tab__content', false);
  var accordionInner = new Accordion($('.js-tab-content-inner'), '.b-main-tab__content', false, '.js-accordion-inner');
  var accordionTeacher = new Accordion($('.js-teacher-accordion'), '.js-accordion-content', false);
});


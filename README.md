eslint - JavaScript Style Guide от Airbnb

Оригинал <a href='https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb'>https://github.com/airbnb/javascript/tree/master/packages/eslint-config-airbnb </a> <br>
Перевод <a href='https://github.com/leonidlebedev/javascript-airbnb'>https://github.com/leonidlebedev/javascript-airbnb</a>

В проекте использается сочетание gulp && webpack, они расположены в папке task

Папка img_min содержит оригиналы картинок, в папку public/images попадают оптимизированные изображение(с помощью сборщика) <br>
Использвется шаблонизатор pug, который генерирует готовые html страницы в папку public<br>
Для стилей использается SCSS<br>
Для страницы печати подключены отдельные стили, расположены в public/css/print.css<br>
Для страницы гайдлайна подключены отдельные стили, расположены в public/css/guide.css<br>
Папка public - для продакшена<br>
Папка svg-sprite - расположены файлы для сборки спрайта<br>
<br>
Команды для запуска:<br>
npm run dev - режим разработки<br>
npm run build:prod - режим продакшена<br>
<br>
<br>
<br>
Гайдлайн для Текстовой страницы
Распложен в public/guide.html (не генерируется pug"ом")<br>
Для это файла требуется папка с js-guide c скриптами






